module Catarotoid.Morphs where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

import Catarotoid.BaseTools
import Catarotoid.Types

emptyJ _ = [True]
emptyJM _ = return [True]


focMsh msh = case msh of
               Nothing -> focuRoot
               Just sh -> focuShare sh

focusUp1 a | null c = b
           | otherwise = allSharesToPositive $ removeDoubleFocus $ focusUp2 d
 where
  b = negateSecondShare $ focusUp2 a
  c = map fromShar $ getSharesConnectedToFocus b
  d = foldr focuShare b c

focusUp = focusUp0

removeDoubleFocus a = morph f emptyJ a
 where
  f _ (Focu (Focu a e) _) = Focu a e
  f _ a = a

removeFocusAfterRedu a = morph f emptyJ a
 where
  f _ (Focu (Redu a) _) = Redu a
  f _ a = a

focusUp0 a = morph f j a
 where
  --j (Focu (Redu _)) = [False]
  j (Focu _ _) = [True]
  j _        = [False]
  f _    o@(Focu _ _) = o
  f a c | or a = Focu c E
  f _    a          = a

focusUp2 a = removeFocusAfterRedu $ morph f j a
 where
  j (Focu (Redu _) _) = [False]
  j (Focu _ _) = [True]
  j _        = [False]
  f _    o@(Focu _ _) = o
  f a c | or a = Focu c E
  f _    a          = a

focuShare :: Int -> Term -> Term
focuShare shr a = morph f emptyJ a
 where
  f _ (Shar a) | a == shr = Focu (Shar a) E
  f _ a = a

focuRoot :: Term -> Term
focuRoot a = morph f emptyJ a
 where
  f _ a@Root = Focu Root E
  f _ a = a

focu n (a,b) = (morph f emptyJ a,b)
 where
  f _ (Atom b) | n == b = Focu (Atom b) E
  f _ a = a

getShares o@(Shar _) = [o]
getShares a = list
 where
  User _ list = morph f j a
  j o@(Shar b) = [o]
  j (User 0 a) = a
  j _ = []
  f _ o@(Shar _) = o
  f a _ = User 0 a

getSharesConnectedToFocus a = filter (isConnectedToFocus a) $ getShares a

isConnectedToFocus a (Shar n) = isDoubleFocusExist $ focusUp2 $ focuShare (negate n) a

isDoubleFocusExist a = b == User 1 []
 where
  b = morph f j a
  j (User 1 _) = [User 1 []]
  j (Focu (User 1 _) _) = [User 1 []]
  j o@(Focu _ _) = [o]
  j _ = []
  f [Focu _ _,Focu _ _] _ = User 1 []
  f _ (Focu (User 1 []) _) = User 1 []
  f _ o@(Focu _ _) = o
  f a _ | User 1 [] `elem` a = User 1 []
  f a b = b

negateSecondShare a = fst $ runState (morphM f emptyJM a) []
 where
  f :: [Bool] -> Term -> StateT [Int] Identity Term
  f _ (Shar a) = do s <- get
                    case abs a `elem` s of
                      True  -> return (Shar (negate $ abs a))
                      False -> modify (abs a :) >> return (Shar $ abs a)
  f _ a = return a

allSharesToPositive a = morph f emptyJ a
 where
  f _ (Shar a) = Shar $ abs a
  f _ a = a
  


embedShare sh a b = morph f emptyJ b
 where
  f _ (Shar b) | b == sh = Inve a
  f _ a = a

onlyUser a = morph f j a
 where
  j o@(User a b) = [o]
  j _ = []
  f [a] _ = a
  f _ a = a

countInverts a = b
 where
  User b _ = c
  c = morph f j a
  j o@(User _ _) = [o]
  j _ = []
  f [User a _] (Inve _) = User (a+1) []
  f [User a _] (Favo _) = User (a+1) []
  f [o@(User _ _)] _ = o
  f [] (Focu Root _) = User 0 []
  f [] (Focu (Shar _) _) = User 1 []
  f _ a = a

isRootRotation a = uncurry (||) $ tmap (isRoot . morph f j) a
 where
  j Root = [Root]
  j _    = []
  f [Root] b = Root
  f _ b = b

simpleReduce a = morph f emptyJ a
 where
  sr = simpleReduce
  f _ (Inve (a `Concat` b)) = sr $ Inve a `Concat` Inve b
  f _ (Inve (a `Times` b)) = sr $ Inve a `Times` Inve b
  f _ (Favo (Inve (Favo (Inve a)))) = a
  f _ (Inve (Favo (Inve (Favo a)))) = a
  f _ (Favo (Inve (Favo a))) = Inve a
  f _ (Inve (Favo (Inve a))) = Favo a
  f _ (Favo (Favo a)) = a
  f _ (Favo (a `Concat` b)) = sr $ Favo b `Concat` Favo a
  f _ (Unit `Times` a) = a
  f _ (a `Times` Unit) = a
  f _ (Unit `Concat` a) = a
  f _ (a `Concat` Unit) = a
  f _ (Inve (Inve a)) = a
  f _ a = a -- traceShow a a

isRotatedAtomInvertedIn :: Maybe Int -> Term -> Bool
isRotatedAtomInvertedIn msh term = mIntToBool msh $ countInverts $ focusUp $ focMsh msh term


isInvertedIn :: Maybe Int -> Int -> Term -> Bool
isInvertedIn msh sh a = ((c+p) `mod` 2) == 1
 where
  p = if isJust msh then 1 else 0
  b = focusUp $ focMsh msh $ focuShare sh a
  User c _ = onlyUser $  morph fun jun b

  fun [o@(User _ _)] _ = o
  fun  _ a = a

  jun a | isJust ro = case fromJust ro of
    (Focu b e1,Focu c e2,f,_) -> [User (countInverts (Focu b e1) + countInverts (Focu c e2)) []]
    _                   -> []
   where
    ro = rotOp a
  jun _ = []






