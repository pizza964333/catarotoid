{-# LANGUAGE TupleSections #-}

module Catarotoid.SmallTools where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

import Catarotoid.Types
import Catarotoid.BaseTools
import Catarotoid.Morphs

a n = Atom n
i n = Inve $ Atom n

foca (a,b) = (focusUp a, b)

shoca sh (a,b) = (focusUp $ focuShare sh a, b)


swap (a,b) = (simpleReduce b,simpleReduce a)
normS (a,b) = (simpleReduce a,simpleReduce b)


embsh sh e (a,b) = (embedShare sh e a, b)


getSharIds a = map fromShar $ concat $ tmap getShares a











