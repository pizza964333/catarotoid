module Catarotoid.TestExamples where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

import Catarotoid.Types
import Catarotoid.Tools
import Catarotoid.SmallTools
import Catarotoid.GraphViz
import Catarotoid.Morphs

test01 = ((a 1 `Concat` a 2 `Concat` a 3 `Concat` a 4 `Concat` a 5 `Concat` a 6 `Concat` a 7 `Concat` a 8) `Times` a 9 `Times` a 10, Unit)

test02 = a 1 `Times` i 1 `Concat` a 2 `Times` i 2

test03 = (Favo (Atom 2) `Concat` Favo ((Focu (Atom 1) E)) `Concat` Favo (Atom 4 `Concat` (Atom 5 `Concat` Atom 6)),Atom 3)

test04 = (a 1 `Concat` a 2 `Concat` a 3 `Concat` a 4 `Concat` a 5 `Concat` a 6 `Concat` a 7 `Concat` a 8) `Times` a 9 `Times` a 10

test05 = [(foldr1 Times [b,c,d,e,i 2],Root)]
 where
  b = a 1 `Concat` a 2 `Concat` a 3
  c = i 3 `Concat` a 4 `Concat` a 5 `Concat` a 2
  d = i 1 `Concat` i 5
  e = i 2 `Concat` i 4

testNet01 = map normS $ [a,d] ++ ( rotate $ foca $  focu 4 $ toUnitForm (b,c) )
 where
  a = (Atom 2,(Favo (Atom 1) `Concat` Inve (Shar 4200723695)) `Concat` Favo (Atom 3))
  b = Favo (Atom 5) `Concat` (Favo (Atom 4) `Concat` (Favo (Inve (Atom 3)) `Concat` (Shar 4200723695 `Times` Inve (Shar 2328830743))))
  c = Inve (((Atom 1 `Concat` Atom 5 `Times` Shar 2328830743) `Times` Inve (Shar 3774032922)) `Concat` Favo (Inve (Atom 4)))
  d = (Atom 2,Inve (Shar 3774032922))


