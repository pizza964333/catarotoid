{-# LANGUAGE TupleSections, RankNTypes, MultiParamTypeClasses, FlexibleInstances, FlexibleContexts #-}

module Catarotoid.BaseTools where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

tmap f (a,b) = (f a, f b)

mayIter f a = case f a of
  Nothing -> a
  Just a  -> mayIter f a

appendHeadToList :: (a -> b) -> [a] -> [b] -> [b]
appendHeadToList f a b = if null a then b else f (head a) : b

onlyNonDup a = foldr delete a $ (a \\ nub a)

swp (a,b) = (b,a)

xor True True = False
xor _    True = True
xor True _    = True
xor _    _    = False

icatZip :: [[(Int,a)]] -> [b] -> [(a,b)]
icatZip [] _ = []
icatZip (a:b) list = result ++ icatZip b nList
 where (result,nList) = foldr (\(c,d) (e,list) -> ((d,list !! c) : e, deleteIndex c list)) ([],list) a

deleteIndex :: Int -> [a] -> [a]
deleteIndex n a = map fst $ filter (\(a,b) -> b /= n) $ zip a [0..]

deleteIndexes :: [Int] -> [a] -> [a]
deleteIndexes ns a = map fst $ filter (\(a,b) -> not $ b `elem` ns) $ zip a [0..]

mIntToBool :: Maybe a -> Int -> Bool
mIntToBool a b = mod (maybeToNum a + b) 2 == 1

maybeToNum :: Num a => Maybe b -> a
maybeToNum (Just _) = 1
maybeToNum       _  = 0


rotations list = b
 where
  a = map swp $ zip list [0..]
  b = map (map snd) $ map sort $ map (\n -> map (\(a,b) -> (abs (a-n),b)) a) [0..(length list - 1)]








