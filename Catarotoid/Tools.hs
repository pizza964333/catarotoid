{-# LANGUAGE TupleSections #-}

module Catarotoid.Tools where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

import Catarotoid.Types
import Catarotoid.BaseTools
import Catarotoid.SmallTools
import Catarotoid.Morphs

tryReducePolarInverts (a,b) = undefined
 where
  c = [ tryReduce (d,e) | d <- rotations a, e <- rotations b ]

tryReduce (dt:dne,it:ine) = undefined
 where
  net = dne++ine

netFocusUp :: [(Term,Term)] -> [(Term,Term)]
netFocusUp net = undefined

splitToPolarInverts :: [(Term,Term)] -> ([(Term,Term)],[(Term,Term)])
splitToPolarInverts net = tmap (map snd) (leftFalse,rightTrue)
 where
  Just rootRotation      = find isRootRotation net
  polarityWithRotation   = icatZip totalPolarities $ rootRotation : rootLessRotations
  totalPolarities        = [(0,isRotatedAtomInvertedIn Nothing $ snd rootRotation)] : next rootSharesWithPolarity (rootLessShares,rootLessRotations)
  rootSharesWithPolarity = map (\sh -> (sh, isInvertedIn Nothing sh $ snd rootRotation)) $ onlyNonDup $ getSharIds rootRotation
  rootLessShares         = map (onlyNonDup . getSharIds) rootLessRotations
  leftFalse              = filter (not.fst) polarityWithRotation
  rightTrue              = filter      fst  polarityWithRotation
  rootLessRotations      = rootRotation `delete` net
  next :: [(Int,Bool)] -> ([[Int]],[(Term,Term)]) -> [[(Int,Bool)]]
  next [] _ = []
  next a o@(b,rots) = polarities : next foundedSharesWithPolarity (delfi b, delfi rots)
   where
    sharesConnectedToRotations = foldr collectConnectedRotations [] a :: [((Int,Bool),Int)] where
       collectConnectedRotations a ls = appendHeadToList (a,) rotationsThatHasConnection ls where
           rotationsThatHasConnection = foldr (\b s -> if fst a `elem` fst b then snd b : s else s) [] $ zip b [0..]
    delfi = deleteIndexes foundedIndexes
    polarities = map (\((sh,bl),b) -> (b,xor bl $ isRotatedAtomInvertedIn (Just sh) $ snd (rots !! b) ) ) sharesConnectedToRotations
    foundedIndexes = map fst polarities
    foundedSharesWithPolarity = concat $ map (\o@((sh,bl),n) -> map (\a->(a,calculatePolarity o a)) $ delete sh (b !! n) ) sharesConnectedToRotations
      where calculatePolarity ((sh,bl),n) nsh = xor bl $ isInvertedIn (Just sh) nsh $ snd (rots !! n)

rotate (Inve a,b) = rotate (a,Inve b)
rotate (Focu a _,b) = rotate (a,b)
rotate (Redu a,b) = rotate (a,Redu b)
rotate (Favo (Focu a _), b) = rotate (a,Favo b)
rotate q@(o,c) | isJust ro = case fromJust ro of
  (Focu a _, Focu b _,f,i) -> unsafePerformIO $ do
    d <- randomRIO (2^31,2^32-1)
    let e = (a,c `f` i (Shar d))
    let g = (b,Shar d)
    return $ rotate e ++ rotate g
  (Focu a _, b,f,i) -> rotate (a,sr (c `f` i b))
  (a, Focu b _,f,i) -> rotate (b,sr (i a `f` c))
  _               -> [q]
 where
  sr = simpleReduce
  ro = rotOp o
rotate a = [a]




multiFocus [] c = [c]
multiFocus a b = rotate $ foca $ foldr (\a b -> focu a b) b a

walk [] c = c
walk (a:b) c = normS $ walk b (swap $ head $ rotate $ foca $ focu a c)

reduceNet (Net rots) = rtwsh
 where
  shares = map (tmap getShares) rots
  rtwsh = zip shares rots

tryRewSh :: Int -> ((Term,Term),(Term,Term)) -> ((Term,Term),(Term,Term))
tryRewSh shr (rot1,rot2) = ((a,e),(c,g))
 where
  (a,b) = tmap (focusUp . focuShare shr) rot1
  (c,d) = tmap (focusUp . focuShare shr) rot2
  (e,g) = fst $ runState (do a <- morphM (fb b) emptyJM d
                             s <- get
                             case s of
                               (Just b,_) -> return (a,b)
                               _ -> return (b,d)
                  ) (Nothing,(limL,limR))
  limL = 4
  limR = 4
  predL  (a,(b,c)) = (a,(b-1,limR))
  predR  (a,(b,c)) = (a,(b,c-1))
  fb :: Term -> [Bool] -> Term -> State (Maybe Term,(Int,Int)) Term
  fb b _ (Focu a _) = do
    s <- get
    case s of
      (_,(n,_)) | n <= 0 -> return a
                | n == limL -> modify predL >> return a
      (Just _,_) -> return a
      _      -> do modify predL
                   d <- morphM f emptyJM b
                   c <- get
                   case c of
                     (Nothing,_) -> return a
                     (Just a,_)  -> put (Just d,(0,0)) >> return a
   where
    f :: [Bool] -> Term -> State (Maybe Term,(Int,Int)) Term
    f _ (Focu b _)=do s <- get
                      case (s,sharedReduce (a,b)) of
                        ((_,(_,n)),_) | n <= 0 -> return b
                                      | n == limR -> modify predR >> return b
                        (_,Just (a,b)) -> put (Just a,(0,0)) >> return b
                        (_,_)    -> modify predR >> return b
    f _ a = return a
  fb _ _ a = return a
      
-- if only one share in term shares always equal in pair
sharedReduce (Inve (Shar a), Inve (Shar b)) = Just (Shar a, Shar b)
sharedReduce a = Nothing


toUnitForm (a,b) = unsafePerformIO $ do
  c <- randomRIO (2^48,2^62)
  let d = Shar c
  let (_,e) = head $ rotate $ foca $ (a `Times` (Focu d E),b)
  return (e,Unit)

normalFormForNet :: [(Term,Term)] -> [(Term,Term)]
normalFormForNet a = b
 where
  b = map normS $ mayIter embedShares $ map toUnitForm a

embedShares a
  | isJust c && not (null e) && not (null hh) = Just ([q] ++ p ++ k)
  | otherwise = Nothing
 where
  b = zip (map (tmap getShares) a) a
  f ((a,b),_) = let c = a ++ b in length c == length (nub c)
  c = find f b
  Just d = c
  e = fst (fst d) ++ snd (fst d)
  Shar g = head e
  hh = filter (\((a,b),_) ->  g `elem` map fromShar (a++b) ) $ d `delete` b
  kk = foldr (\a b -> delete a b) b (d:hh)
  h2 = map snd hh
  k = map snd kk
  h = head $ rotate $ shoca g $ toUnitForm $ head h2
  p = tail h2
  q = normS $ embsh g (snd h) (toUnitForm $ snd d)


rewrite 0 a = return a
rewrite n netList = do
  a <- getRandomAtom netList
  let b = getOppositeAtoms a netList
  let c = filter (ableToRewrite a) b
  if null c then rewrite (n-1) netList else do
    d <- randomRIO (0,length c - 1)
    let e = c !! d
    rewriteWithPair netList a e

rotateToRandomAtom a =
  case map toUnitForm $ normalFormForNet a of
    [(a,Unit)] -> rotateTermToRandomAtom a
    a -> error $ show ("RTRA",a)

rotateTermToRandomAtom a = rotate $ foca $ focu c (a,Unit)
 where
  b = morph f1 emptyJ a
  (User _ [Atom c]) = morph f2 j2 b
  --d = simpleReduce $ morph f3 emptyJ b
  f1 _ (Atom a) = unsafePerformIO $ do
    b <- randomRIO (2^48,2^61)
    return $ User b [Atom a]
  f1 _ a = a
  j2 o@(User _ _) = [o]
  j2 _ = []
  f2 [] _ = Unit
  f2 a b = foldr1 amax a
{-
  User cc _ = c
  f3 _ (User a (Atom b)) | a == cc = Atom b
                         | otherwise = Unit
  f3 _ a = a
-}
  amax e@(User a b) g@(User c d) = if a > c then e else g







{-
test123 = (Inve ((Favo (Inve (Atom 1)) `Concat` (Inve Root `Times` Inve (Shar 2816593068))) `Concat` Favo (Inve (Atom 3))),Atom 2)
test124 = Focu (Focu (Inve (Focu Root)))
test125 = Focu (Inve (Focu (Shar 2816593068)))
test126 = [(Atom 2,Inve ((Favo (Inve (Atom 1)) `Concat` (Inve Root `Times` Inve (Shar 2538154003))) `Concat` Favo (Inve (Atom 3)))),(Atom 2,Inve (Favo (Inve (Atom 5)) `Concat` (Favo (Inve (Atom 4)) `Concat` (Favo (Atom 3) `Concat` (Shar 2538154003 `Times` Inve (Shar 3613665129)))))),(Atom 2,((Inve (Atom 1) `Concat` Inve (Atom 5) `Times` Shar 3613665129) `Times` Inve (Shar 2200729293)) `Concat` Favo (Atom 4)),(Atom 2,Shar 2200729293)]
-}

testNet02 = [(Atom 2,Inve ((Favo (Inve (Atom 1)) `Concat` (Inve Root `Times` Inve (Shar 3713173222))) `Concat` Favo (Inve (Atom 3)))),(Inve (Favo (Inve (Atom 5)) `Concat` (Favo (Inve (Atom 4)) `Concat` (Favo (Atom 3) `Concat` (Shar 3713173222 `Times` Inve (Shar 3350044189))))),Redu (((Inve (Atom 1) `Concat` Inve (Atom 5) `Times` Shar 3350044189) `Times` Inve (Shar 3794736852)) `Concat` Favo (Atom 4))),(Atom 2,Shar 3794736852)]



getRandomAtom = undefined
{-
нужно получить адрес атома, это проблема
выбрать атом и повернуть сеть чтобы атом был в фокусе


-}



getOppositeAtoms = undefined
ableToRewrite = undefined
rewriteWithPair = undefined






