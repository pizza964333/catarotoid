module Catarotoid.Types where

import System.Random
import System.IO.Unsafe
import Data.Maybe
import Debug.Trace
import Data.Functor.Identity
import Control.Monad.State.Lazy
import Data.List
import Data.IORef

data Info = E  -- empty, no information
          | TP --      top polarity
          | IP -- inverted polarity
 deriving (Read,Show,Eq)

data Term
 = Root
 | Unit
 | Atom Int
 | Shar Int
 | Term `Times`  Term
 | Term `Concat` Term
 | Term `Accept` Term
 | Nega Term
 | Ofco Term
 | Inve Term
 | Favo Term
 | Focu Term Info
 | Redu Term

 | SBus Int  [Wire]
 | Pipe Term [Wire]

 | FanBBAT Int Term Term
 | FanLBYR Int Term Term
 | FanBRYL Int Term Term

 | User Int [Term]

 deriving (Read,Show,Eq)

data Wire = Wire | Crois | Brkt Wire Wire | BrktL | BrktR
  deriving (Read,Show,Eq)

data Net = Net [(Term,Term)]
 deriving (Show)

infixr 9 `Concat`
infixr 8 `Times`
infixr 7 `Accept`

wireLamVar a = a `Pipe` [Brkt Wire BrktL,BrktR,Wire]
wireLamApp a b = FanBRYL 1 (a `Pipe` [Wire,Crois,Wire]) (b `Pipe` [Wire,Brkt Wire Wire]) `Pipe` [Wire,BrktL,BrktR]
wireLamAbst a = do
  sh <- randomRIO (2^32,2^62)
  return $ FanBBAT 1 (a `Pipe` [Wire,Brkt Wire Wire]) (SBus sh [Wire,Brkt Wire Wire]) `Pipe` [BrktL,BrktR,Wire]


morph :: ([a] -> Term -> Term) -> (Term -> [a]) -> Term -> Term
morph f j a = runIdentity $ morphM (\a b -> return $ f a b) (return.j) a

morphM :: Monad m => ([a] -> Term -> m Term) -> (Term -> m [a]) -> Term -> m Term
morphM f j (a `Times`  b) = do c <- morphM f j a; d <- morphM f j b; e <- j c; g <- j d; f (e ++ g) $ c `Times`  d
morphM f j (a `Concat` b) = do c <- morphM f j a; d <- morphM f j b; e <- j c; g <- j d; f (e ++ g) $ c `Concat` d
morphM f j (Ofco a)       = do b <- morphM f j a; c <- j b; f c $ Ofco b
morphM f j (Inve a)       = do b <- morphM f j a; c <- j b; f c $ Inve b
morphM f j (Favo a)       = do b <- morphM f j a; c <- j b; f c $ Favo b
morphM f j (Focu a e)       = do b <- morphM f j a; c <- j b; f c $ Focu b e
morphM f j (Redu a)       = do b <- morphM f j a; c <- j b; f c $ Redu b
morphM f j a              = do b <- j a; f b a

rotOp (a `Concat` b) = Just (a,b,Concat,Favo)
rotOp (a `Times`  b) = Just (a,b,Times ,Inve)
rotOp (a `Accept` b) = Just (a,b,Accept,Nega)
rotOp _              = Nothing


fromShar (Shar a) = a

isUser (User _ _) = True
isUser _ = False

isRoot Root = True
isRoot _    = False









